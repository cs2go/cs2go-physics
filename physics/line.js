const Point = require('./point');

module.exports = class Line {
	constructor(start = new Point(), end = new Point()) {
		this.start = start;
		this.end = end;
	}

	get length() {
		return Point.distance(this.start, this.end);
	}

	get delta() {
		return Line.delta(this.end, this.start);
	}

	rotate(angle) {
		this.end.sub(this.start).rotate(angle).add(this.start);
		return this;
	}

	rotateBy(angle) {
		this.end.clone().sub(this.start).rotateBy(angle).add(this.start);
		return this;
	}

	clone() {
		return new Line(this.start.clone(), this.end.clone());
	}

	reflect(line) {
		let out = { x: 0, y: 0 };

		let vec = line.delta;
		let norm = this.delta.normal;
		let dot = this.dotProductOf(line.start);

		out.x = line.x - 2 * this.dotProductOf()
	}

	static delta(p1, p2) {
		return Point.sub(p1, p2);
	}

	dotProductOf(point) {
		if (!(point instanceof Point)) throw new TypeError('point must be of type Point')

		let dotCalc = (
			((point.x - this.start.x) * (this.end.x - this.start.x)) +
			((point.y - this.start.y) * (this.end.y - this.start.y))
		);

		let dotProduct = 0;
		if (dotCalc != 0)
			dotProduct = dotCalc / Math.pow(this.length, 2);

		return new Point(
			this.start.x + (dotProduct * (this.end.x - this.start.x)),
			this.start.y + (dotProduct * (this.end.y - this.start.y))
		);
	}
}