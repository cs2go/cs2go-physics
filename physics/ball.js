const Point = require('./point');

module.exports = class Ball extends Point {
	static fromPoint(point, radius = 25) {
		return new Ball(point.x, point.y, radius);
	}

	constructor(x, y, radius = 25) {
		super(x, y);
		this.radius = radius;
	}

	add(point) {
		if (point instanceof Point) {
			let nPos = Point.add(this, point);
			this.x = nPos.x;
			this.y = nPos.y;
		}

		return this;
	}

	clone() {
		return new Ball(this.x, this.y, this.radius);
	}

	get asPoint() {
		return new Point(this.x, this.y);
	}
}