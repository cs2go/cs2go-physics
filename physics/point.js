const degPICalc = 180 / Math.PI;

const rad2deg = radians => radians * degPICalc;
const deg2rad = degrees => degrees / degPICalc;

const testNumber = (val, prop) => {
	if (typeof val !== 'number' || isNaN(val))
		throw new TypeError(`'${prop}' is not a number`);
}

const testPoint = point => {
	testNumber(point.x, 'x');
	testNumber(point.y, 'y');
}

module.exports = class Point {
	constructor(x = 0, y = 0) {
		this.x = x;
		this.y = y;
	}

	set x(val) {
		testNumber(val, 'x');
		this._x = val;
	}
	get x() { return this._x }

	set y(val) {
		testNumber(val, 'y');
		this._y = val;
	}
	get y() { return this._y }

	get length() {
		return Math.sqrt(
			(this.x * this.x) +
			(this.y * this.y)
		);
	}

	get normal() {
		let nX = this.x == 0 || this.length == 0 ? 0 : this.x / this.length;
		let nY = this.y == 0 || this.length == 0 ? 0 : this.y / this.length;

		return new Point(nX, nY);
	}

	get angle() {
		return rad2deg(Math.atan2(this.x, this.y));
	}


	get left() {
		return new Point(this.x * -1, this.y);
	}
	get right() {
		return new Point(this.x, this.y * -1);
	}

	get reverse() {
		return new Point(this.x * -1, this.y * -1);
	}


	limit(limit) {
		testNumber(limit);

		if (this.length > limit) {
			let nPoint = Point.limit(this, limit);
			this.x = nPoint.x;
			this.y = nPoint.y;
		}

		return this;
	}

	static limit(point, limit) {
		testPoint(point, 'point');
		testNumber(limit, 'limit');

		return point.length > limit ?
			Point.multi(point.normal, limit) :
			point;
	}


	clone() {
		return Point.from(this);
	}

	static from(obj) {
		testPoint(obj, 'obj');
		return new Point(obj.x, obj.y);
	}


	add(point) {
		let nPoint = Point.add(this, point);
		this.x = nPoint.x;
		this.y = nPoint.y;

		return this;
	}

	static add(p1, p2) {
		testPoint(p1, 'p1');
		testPoint(p2, 'p2');

		return new Point(p1.x + p2.x, p1.y + p2.y);
	}


	sub(point) {
		let nPoint = Point.sub(this, point);
		this.x = nPoint.x;
		this.y = nPoint.y;

		return this;
	}

	static sub(p1, p2) {
		testPoint(p1, 'p1');
		testPoint(p2, 'p2');

		return new Point(p1.x - p2.x, p1.y - p2.y);
	}


	divide(magnitude) {
		testNumber(magnitude);

		let nPoint = Point.divide(this, magnitude);
		this.x = nPoint.x;
		this.y = nPoint.y;

		return this;
	}

	static divide(p1, magnitude) {
		testPoint(p1, 'p1');
		testNumber(magnitude, 'magnitude');

		return new Point(p1.x / magnitude, p1.y / magnitude);
	}


	multi(magnitude) {
		testNumber(magnitude);

		let nPoint = Point.multi(this, magnitude);
		this.x = nPoint.x;
		this.y = nPoint.y;

		return this;
	}

	static multi(p1, magnitude) {
		testPoint(p1, 'p1');
		testNumber(magnitude, 'magnitude');

		let sum = { x: p1.x, y: p1.y };
		if (p1.x !== 0) sum.x *= magnitude;
		if (p1.y !== 0) sum.y *= magnitude;
		return new Point(sum.x, sum.y);
	}


	distance(point) {
		testPoint(point);

		return Point.distance(this, point);
	}

	static distance(p1, p2) {
		testPoint(p1, 'p1');
		testPoint(p2, 'p2');

		if (p1.length > p2.length) {
			return Point.sub(p1, p2).length;
		} else {
			return Point.sub(p2, p1).length;
		}
	}


	rotate(angle) {
		testNumber(angle);

		let nPoint = Point.rotate(this, angle);
		this.x = nPoint.x;
		this.y = nPoint.y;
		return this;
	}

	rotateBy(angle) {
		testNumber(angle);

		let nPoint = Point.rotate(this, this.angle + 180 + angle);
		this.x = nPoint.x;
		this.y = nPoint.y;
		return this;
	}

	static rotate(point, angle) {
		testPoint(point, 'point');
		testNumber(angle, 'angle');

		let radians = deg2rad(angle);

		let cos = Math.cos(radians);
		let sin = Math.sin(radians);

		return new Point(
			(cos * point.x) + (sin * point.y),
			(cos * point.y) - (sin * point.x)
		);
	}
}