const Point = require('./point');
const Line = require('./line');

module.exports = class Rect {
	constructor(p1 = new Point(0, 0), p2 = new Point(1, 0), p3 = new Point(1, 1), p4 = new Point(0, 1)) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.p4 = p4;
	}

	get line1() {
		return new Line(this.p1, this.p2);
	}

	get line2() {
		return new Line(this.p2, this.p3);
	}

	get line3() {
		return new Line(this.p3, this.p4);
	}

	get line4() {
		return new Line(this.p4, this.p1);
	}

	get leftBound() {
		return Math.min(Math.min(this.p1.x, this.p2.x), Math.min(this.p3.x, this.p4.x));
	}

	get rightBound() {
		return Math.max(Math.max(this.p1.x, this.p2.x), Math.max(this.p3.x, this.p4.x));
	}

	get topBound() {
		return Math.min(Math.min(this.p1.y, this.p2.y), Math.min(this.p3.y, this.p4.y));
	}

	get bottomBound() {
		return Math.max(Math.max(this.p1.y, this.p2.y), Math.max(this.p3.y, this.p4.y));
	}

	get center() {
		return this.p1.clone()
			.add(this.p2)
			.add(this.p3)
			.add(this.p4)
			.divide(4);
	}

	clone() {
		return new Rect(
			this.p1.clone(),
			this.p2.clone(),
			this.p3.clone(),
			this.p4.clone()
		);
	}

	add(offset) {
		this.p1.add(offset);
		this.p2.add(offset);
		this.p3.add(offset);
		this.p4.add(offset);

		return this;
	}

	rotate(angle) {
		let center = this.center;

		let np1 = this.p1.clone().sub(center);
		np1.rotateBy(angle);
		this.p1 = np1.add(center);

		let np2 = this.p2.clone().sub(center);
		np2.rotateBy(angle);
		this.p2 = np2.add(center);

		// let np3 = this.p3.clone().sub(center);
		// np3.rotateBy(angle);
		// this.p3 = np3.add(center);

		// let np4 = this.p4.clone().sub(center);
		// np4.rotateBy(angle);
		// this.p4 = np4.add(center);

		//	not completly tested
		// debugger;
		console.warn('Rect.prototype.rotate has not been completed yet and still shows bugs');
	}

	static fromWidthHeight(pos, size) {
		return new Rect(
			new Point(pos.x, pos.y),
			new Point(pos.x + size.width, pos.y),
			new Point(pos.x + size.width, pos.y + size.height),
			new Point(pos.x, pos.y + size.height)
		)
	}

	static fromPosSize(x, y, w, h) {
		return new Rect(
			new Point(x, y),
			new Point(x + w, y),
			new Point(x + w, y + h),
			new Point(x, y + h)
		);
	}

	static fromLine(line) {
		return Rect.fromPosSize(
			line.start.x,
			line.start.y,
			line.delta.x,
			line.delta.y
		)
	}
}