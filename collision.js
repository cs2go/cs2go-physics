const Point = require('./physics/point');
const Ball = require('./physics/ball');
const Line = require('./physics/line');
const Rect = require('./physics/rect');

const isPoint = obj => !!obj && obj instanceof Point;
const isLine = obj => !!obj && obj instanceof Line;
const isBall = obj => !!obj && obj instanceof Ball;
const isRect = obj => !!obj && obj instanceof Rect;

/*
 * returns the closest collision from a array,
 * otherwise returns an empty array
 */
const getClosest = (target, collisions) => {
	if (!isPoint(target))
		throw new TypeError('target must be a Point');

	let hits = collisions
		.filter(collition => !!collition)
		.reduce((closest, collision) => {
			if (!closest) return collision;

			let closest_dist = Point.distance(closest, target);
			let hit_dist = Point.distance(collision, target);

			if (closest_dist <= hit_dist) return closest;
			return collision;
		}, undefined);

	if (!hits) return false;
	if (hits instanceof Array && hits.length <= 0) return false;
	if (isPoint(hits)) return hits
	return false;
}

/* 
 * detects a collision of a point and a point with a radius
 * returns true if occured, otherwise false
 */
const pointBall = (point, ball) => {
	if (!isPoint(point))
		throw new TypeError('point must be type of Point');

	if (!isBall(ball))
		throw new TypeError('radius must be a number');

	return Point.distance(point, ball) < ball.radius;
}

/* 
 * detects a collision of a line and a point (with some buffer distance)
 * returns true if collision occured, otherwise false
 */
const linePoint = (line, point, buffer = .1) => {
	if (!isLine(line))
		throw new TypeError('line must be an instance of Line');

	if (!isPoint(point))
		throw new TypeError('point must be an instance of Point');

	let dist_start = Point.distance(point, line.start);
	let dist_end = Point.distance(point, line.end);
	let dist_sum = dist_start + dist_end;

	return dist_sum <= line.length + buffer && dist_sum <= line.length + buffer
}

/*
 * detects a collision of a line and a point with a radius
 * returns the point on the line if occured, or
 * returns false
 */
const lineBall = (line, ball) => {
	if (!isLine(line)) throw new TypeError('line must be type of Line');
	if (!isBall(ball)) throw new TypeError('ball must be type of Ball');

	let collisions = [];

	let hitAngle = line.delta.angle;

	//	if the dot product is not on the original line
	//	bail out!
	let dot = line.dotProductOf(ball);
	dot.hitAngle = hitAngle;

	//	if the distance from the ball
	//	to the dot is smaller than the
	//	ball's radius, the extended line enters the ball
	if (pointBall(dot, ball)) {

		//	if the dot is also on the line,
		//	then we have a collision somewhere in the middle of the line
		if (linePoint(line, dot)) {
			collisions.push(dot);
		}

		let length = Point.distance(ball, dot);
		let distanceToArc = Math.sqrt(ball.radius * ball.radius - length * length);

		let entry = line.start.clone().sub(dot).limit(distanceToArc).add(dot);
		let exit = line.end.clone().sub(dot).limit(distanceToArc).add(dot);

		//	only add tangent line points if it is both
		//	on the line AND inside the ball

		if (linePoint(line, entry)) collisions.push(entry);
		else if (pointBall(line.start, ball)) collisions.push(line.start.clone());

		if (linePoint(line, exit)) collisions.push(exit);
		else if (pointBall(line.end, ball)) collisions.push(line.end.clone());
	}

	if (collisions.length) return collisions;
	return false;
}


/* 
 * detects a collision between two lines,
 * returns a point where the collision occured, or
 * returns false for no collision
 */
const lineLine = (l1, l2) => {
	if (!isLine(l1))
		throw new TypeError('l1 must be type of Line');

	if (!isLine(l2))
		throw new TypeError('l2 must be type of Line');

	let uA = (
		(l2.end.x - l2.start.x) *
		(l1.start.y - l2.start.y) -
		(l2.end.y - l2.start.y) *
		(l1.start.x - l2.start.x)
	) / (
			(l2.end.y - l2.start.y) *
			(l1.end.x - l1.start.x) -
			(l2.end.x - l2.start.x) *
			(l1.end.y - l1.start.y)
		);

	let uB = (
		(l1.end.x - l1.start.x) *
		(l1.start.y - l2.start.y) -
		(l1.end.y - l1.start.y) *
		(l1.start.x - l2.start.x)
	) / (
			(l2.end.y - l2.start.y) *
			(l1.end.x - l1.start.x) -
			(l2.end.x - l2.start.x) *
			(l1.end.y - l1.start.y)
		);

	let intersect = (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1);
	if (!intersect) return false;

	let hit = new Point(
		l1.start.x + (uA * (l1.end.x - l1.start.x)),
		l1.start.y + (uA * (l1.end.y - l1.start.y))
	);

	hit.hitAngle = l2.delta.angle;
	return hit;
}

/*
 * detects a collision between two balls
 * returns true when collisions occured, otherwise false
 */
const ballBall = (ball1, ball2) => {
	if (!isBall(ball1)) throw new TypeError('ball1 must be type of Ball');
	if (!isBall(ball2)) throw new TypeError('ball2 must be type of Ball');

	let distance = Point.sub(ball1, ball2);

	if (distance.length <= ball1.radius + ball2.radius) {
		return distance.normal
			.multi(ball2.radius)
			.add(ball2);
	}

	return false;
}


/*
 * detects a collision between a ball and a rectangle
 * returns the point that the collision occured, otherwise false
 */
const rectBall = (rect, ball) => {
	if (!isRect(rect)) throw new TypeError('rect must be type of Rect');
	if (!isBall(ball)) throw new TypeError('ball must be type of Ball');

	let tests = [
		lineBall(rect.line1, ball),
		lineBall(rect.line2, ball),
		lineBall(rect.line3, ball),
		lineBall(rect.line4, ball)
	];

	return tests.flat().filter(test => !!test);
}


/*
 * detects a collision between a ball and a line
 * returns an array of collisions, otherwise returns an empty array
 */
const rectLine = (rect, line) => {
	if (!isRect(rect)) throw new TypeError('rect must be type of Rect');
	if (!isLine(line)) throw new TypeError('line must be type of Line');

	let tests = [
		lineLine(rect.line1, line),
		lineLine(rect.line2, line),
		lineLine(rect.line3, line),
		lineLine(rect.line4, line)
	];

	return tests.flat().filter(test => !!test);
}


/*
 * returns the area of an object
 * for comparison against another
 */
const physicsAreaOf = object => {
	if (isBall(object)) {
		return {
			x: (object.x - object.radius) - 10,
			y: (object.y - object.radius) - 10,
			w: (object.x + object.radius) + 10,
			h: (object.y + object.radius) + 10
		};
	}

	if (isPoint(object)) {
		return {
			x: object.x - 10,
			y: object.y - 10,
			w: object.x + 10,
			h: object.y + 10
		};
	}

	if (isLine(object)) {
		let lo_x = Math.min(object.start.x, object.end.x);
		let lo_y = Math.min(object.start.y, object.end.y);
		let hi_x = Math.max(object.start.x, object.end.x);
		let hi_y = Math.max(object.start.y, object.end.y);

		return {
			x: lo_x - 10,
			y: lo_y - 10,
			w: hi_x + 10,
			h: hi_y + 10
		};
	}

	if (isRect(object)) {
		return {
			x: object.leftBound,
			y: object.topBound,
			w: object.rightBound,
			h: object.bottomBound
		};
	}
}

/*
 * Return a simple true/false
 * if the items are close enough together to test
 * more intense physics
 */
const test = (object1, object2) => {
	object1 = physicsAreaOf(object1);
	object2 = physicsAreaOf(object2);

	//	object1 in area of object2
	if (object1.x > object2.w) return false;
	if (object1.w < object2.x) return false;
	if (object1.y > object2.h) return false;
	if (object1.h < object2.y) return false;

	return true;
}

module.exports = Object.freeze({
	getClosest,
	test, physicsAreaOf,

	//	ball v ball
	ballBall,

	//	line v ball
	lineBall,

	//	point v point
	//	no op

	// point v ball
	pointBall,

	// point v rect
	// no op

	//	line v point
	linePoint,

	//	line v line
	lineLine,

	//	rect v ball
	rectBall,

	//	rect v line
	rectLine
});